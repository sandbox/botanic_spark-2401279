This module is using Sharejs library for sharing nodes on different social
networks.

INSTALLATION:
Download "Sharejs" library from https://github.com/vutran/share.js and place it
in sites/all/libraries/sharejs

You should have share.js file in path sites/all/libraries/sharejs/src/share.js