<?php
/**
 * @file
 * Admin pages for Share.js module.
 */

/**
 * Configuration page for social.js.
 */
function sharejs_config_form($form, &$form_state) {
  // Check if Share.js library is installed.
  $libraries = libraries_get_libraries();
  if (!isset($libraries['share.js'])) {
    drupal_set_message(t('Please install the share.js library %url.', array('%url' => 'https://github.com/vutran/share.js')), 'error');
    return array();
  }

  $form = array();

  $types = node_type_get_types();
  $type_options = array();
  foreach ($types as $key => $content_type) {
    $type_options[$key] = $content_type->name;
  }

  $form['sharejs_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t('Choose on which content types you would like to place share links.'),
    '#options' => $type_options,
    '#default_value' => array_filter(variable_get('sharejs_types', array())),
  );

  $providers = array(
    'email' => t('Email'),
    'facebook' => t('Facebook'),
    'googleplus' => t('Google Plus'),
    'pinterest' => t('Pinterest'),
    'tumblr' => t('Tumblr'),
    'twitter' => t('Twitter'),
    'reddit' => t('Reddit'),
    'linkedin' => t('LinkedIn'),
    'myspace' => t('My Space'),
  );

  $form['sharejs_providers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available providers'),
    '#description' => t('Choose which providers you want to display on node page.'),
    '#options' => $providers,
    '#default_value' => array_filter(variable_get('sharejs_providers', array())),
  );

  return system_settings_form($form);
}