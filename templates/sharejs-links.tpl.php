<?php
/**
 * @file
 * Template for share links.
 */
?>

<div class="sharejs clearfix">
  <h4><?php print t('Share on social networks'); ?></h4>

  <div class="sharejs-links-wrapper">
    <?php if (isset($facebook)) : ?>
      <?php print $facebook; ?>
    <?php endif; ?>

    <?php if (isset($twitter)) : ?>
      <?php print $twitter; ?>
    <?php endif; ?>

    <?php if (isset($linkedin)) : ?>
      <?php print $linkedin; ?>
    <?php endif; ?>

    <?php if (isset($email)) : ?>
      <?php print $email; ?>
    <?php endif; ?>

    <?php if (isset($googleplus)) : ?>
      <?php print $googleplus; ?>
    <?php endif; ?>

    <?php if (isset($pinterest)) : ?>
      <?php print $pinterest; ?>
    <?php endif; ?>

    <?php if (isset($tumblr)) : ?>
      <?php print $tumblr; ?>
    <?php endif; ?>


    <?php if (isset($reddit)) : ?>
      <?php print $reddit; ?>
    <?php endif; ?>

    <?php if (isset($myspace)) : ?>
      <?php print $myspace; ?>
    <?php endif; ?>
  </div>
</div>
